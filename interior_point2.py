# solutions.py
"""Volume II: Interior Point II (Quadratic Optimization). Solutions file."""

import numpy as np
from scipy import linalg as la
from matplotlib import pyplot as plt
from cvxopt import matrix, solvers
from scipy.sparse import spdiags
from mpl_toolkits.mplot3d import axes3d


def startingPoint(G, c, A, b, guess):
    """
    Obtain an appropriate initial point for solving the QP
    .5 x^T Gx + x^T c s.t. Ax >= b.
    Inputs:
        G -- symmetric positive semidefinite matrix shape (n,n)
        c -- array of length n
        A -- constraint matrix shape (m,n)
        b -- array of length m
        guess -- a tuple of arrays (x, y, mu) of lengths n, m, and m, resp.
    Returns:
        a tuple of arrays (x0, y0, l0) of lengths n, m, and m, resp.
    """
    m,n = A.shape
    x0, y0, l0 = guess

    # Initialize linear system
    N = np.zeros((n+m+m, n+m+m))
    N[:n,:n] = G
    N[:n, n+m:] = -A.T
    N[n:n+m, :n] = A
    N[n:n+m, n:n+m] = -np.eye(m)
    N[n+m:, n:n+m] = np.diag(l0)
    N[n+m:, n+m:] = np.diag(y0)
    rhs = np.empty(n+m+m)
    rhs[:n] = -(G.dot(x0) - A.T.dot(l0)+c)
    rhs[n:n+m] = -(A.dot(x0) - y0 - b)
    rhs[n+m:] = -(y0*l0)

    sol = la.solve(N, rhs)
    dx = sol[:n]
    dy = sol[n:n+m]
    dl = sol[n+m:]

    y0 = np.maximum(1, np.abs(y0 + dy))
    l0 = np.maximum(1, np.abs(l0+dl))

    return x0, y0, l0


# Problems 1-2
def qInteriorPoint(Q, c, A, b, guess, niter=20, tol=1e-16, verbose=False):
    """Solve the Quadratic program min .5 x^T Q x +  c^T x, Ax >= b
    using an Interior Point method.

    Parameters:
        Q ((n,n) ndarray): Positive semidefinite objective matrix.
        c ((n, ) ndarray): linear objective vector.
        A ((m,n) ndarray): Inequality constraint matrix.
        b ((m, ) ndarray): Inequality constraint vector.
        guess (3-tuple of arrays of lengths n, m, and m): Initial guesses for
            the solution x and lagrange multipliers y and eta, respectively.
        niter (int > 0): The maximum number of iterations to execute.
        tol (float > 0): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The optimal point.
        val (float): The minimum value of the objective function.
    """
    #raise NotImplementedError("Problems 1 and 2 Incomplete.")
    def F(x, y, mu):
        f1 = np.dot(Q, x) - np.dot(A.T,mu) + c
        f2 = np.dot(A, x) - y - b
        f3 = np.dot(np.diag(y), np.dot(np.diag(mu), np.ones(len(mu))))
        return np.hstack((f1, np.hstack((f2, f3))))

    # problem 2
    sigma = .1
    def DF(x, y, mu):
        M = np.diag(mu)
        Y = np.diag(y)
        I = np.eye(len(mu))
        Znn = np.zeros_like(I)
        Zmn = np.zeros_like(A)
        Znm = np.zeros_like(A.T)
        Zmm = np.zeros_like(np.diag(y))
        row1 = np.hstack((Q, np.hstack((Znm, -A.T))))
        row2 = np.hstack((A, np.hstack((-I, Zmm))))
        row3 = np.hstack((Zmn, np.hstack((M, Y))))
        return np.vstack((row1, np.vstack((row2,row3))))

    def subroutine1(x, y, mu):
        Zn = np.zeros_like(x)
        Zm = np.zeros_like(y)
        e = np.ones(len(mu))
        nu = np.dot(y, mu) / len(mu)
        vec = np.hstack((Zn,np.hstack((Zm, sigma*nu*e))))
        b = -1 * F(x,y,mu) + vec
        LU = la.lu_factor(DF(x,y,mu))
        return la.lu_solve(LU,b)

    # problem 3
    def subroutine2(x, y, mu):
        ret = subroutine1(x, y, mu)
        n = len(x)
        m = len(y)
        del_x = ret[:n]
        del_y = ret[n:n+m]
        del_mu = ret[n+m:]
        masky = del_y < 0
        maskmu = del_mu < 0
        mu_vec = -mu / del_mu
        y_vec = -y / del_y
        beta_max = min(1, np.min(mu_vec[maskmu]))
        delta_max = min(1, np.min(y_vec[masky]))
        beta = min(1, .95*beta_max)
        delta = min(1, .95*delta_max)
        return min(beta, delta)

    # problem 4
    x, y, mu = startingPoint(Q,c,A,b,guess)
    it = 0
    flag = False
    while np.dot(y, mu) / len(mu) >= tol:
        it += 1
        if it > niter:
            flag = True
            break
        alpha = subroutine2(x, y, mu)
        ret = subroutine1(x, y, mu)
        n = len(x)
        m = len(y)
        del_x = ret[:n]
        del_y = ret[n:n+m]
        del_mu = ret[n+m:]
        x = x + alpha*del_x
        y = y + alpha*del_y
        mu = mu + alpha*del_mu
    return x, np.dot(c,x)



def laplacian(n):
    """Construct the discrete Dirichlet energy matrix H for an n x n grid."""
    data = -1*np.ones((5, n**2))
    data[2,:] = 4
    data[1, n-1::n] = 0
    data[3, ::n] = 0
    diags = np.array([-n, -1, 0, 1, n])
    return spdiags(data, diags, n**2, n**2).toarray()


# Problem 3
def circus(n=15):
    """Solve the circus tent problem for grid size length 'n'.
    Display the resulting figure.
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    H = laplacian(n)

    L = np.zeros((n,n))
    L[n//2-1:n//2+1,n//2-1:n//2+1] = .5
    m = [n//6-1, n//6, int(5*(n/6.))-1, int(5*(n/6.))]
    mask1, mask2 = np.meshgrid(m,m)
    L[mask1,mask2] = .3
    L = L.ravel()

    x = np.ones((n,n)).ravel()
    y = np.ones(n**2)
    mu = np.ones(n**2)

    c = np.ones(n**2)
    c = (-1./((n-1)**2))*c
    A = np.eye(n**2)

    z = qInteriorPoint(H, c, A, L, (x,y,mu))[0].reshape((n,n))

    domain = np.arange(n)
    X,Y = np.meshgrid(domain,domain)
    fig = plt.figure()
    ax1 = fig.add_subplot(111,projection='3d')
    ax1.plot_surface(X,Y,z, rstride=1, cstride=1, color='r')
    plt.show()


# Problem 4
def portfolio(filename="portfolio.txt"):
    """Markowitz Portfolio Optimization

    Parameters:
        filename (str): The name of the portfolio data file.

    Returns:
        (ndarray) The optimal portfolio with short selling.
        (ndarray) The optimal portfolio without short selling.
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    stuff = np.loadtxt(filename)
    mu = np.sum(stuff[:,1:], axis=0)/len(stuff)
    stuff = stuff[:, 1:].T
    Q = np.cov(stuff)
    R = 1.13
    b = np.array([1,R])
    A = np.array([list(np.ones(len(mu))), list(mu)])
    q = np.zeros_like(mu)
    ret1 = solvers.qp(matrix(Q),matrix(q),A=matrix(A),b=matrix(b))

    h = np.zeros_like(mu)
    G = -1*np.eye(len(mu))
    ret2 = solvers.qp(matrix(Q),matrix(q),G=matrix(G), h=matrix(h), A=matrix(A),b=matrix(b))
    return np.array(ret1['x']), np.array(ret2['x'])
