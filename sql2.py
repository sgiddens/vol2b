import sqlite3 as sql
import csv
import pandas as pd

def prob1():
    """
    Specify relationships between columns in given sql tables.
    """
    print "One-to-one relationships:"
    # Put print statements specifying one-to-one relationships between table
    # columns.
    print "5.1"
    print "StudentID and Name"
    print "5.2"
    print "ID and Name"
    print "5.4"
    print "ClassID and Name"


    print "**************************"
    print "One-to-many relationships:"
    # Put print statements specifying one-to-many relationships between table
    # columns.
    print "5.1"
    print "StudentID and MajorCode"
    print "StudentID and MinorCode"
    print "Name and MajorCode"
    print "Name and MinorCode"
    print "5.3"
    print "StudentID and ClassID"
    print "StudentID and Grade"

    print "***************************"
    print "Many-to-Many relationships:"
    # Put print statements specifying many-to-many relationships between table
    # columns.
    print "5.1"
    print "MajorCode and MinorCode"
    print "5.3"
    print "ClassID and Grade"

def prob2():
    """
    Write a SQL query that will output how many students belong to each major,
    including students who don't have a major.

    Return: A table indicating how many students belong to each major.
    """
    #Build your tables and/or query here
    db = sql.connect('sql')
    cur = db.cursor()
    # Creates tables
    cur.execute('DROP TABLE IF EXISTS students')
    cur.execute('DROP TABLE IF EXISTS majors')
    cur.execute('DROP TABLE IF EXISTS grades')
    cur.execute('DROP TABLE IF EXISTS classes')
    cur.execute('CREATE TABLE students (StudentID INT NOT NULL, Name TEXT, MajorCode INT, MinorCode INT);')
    cur.execute('CREATE TABLE majors (ID INT NOT NULL, Name TEXT);')
    cur.execute('CREATE TABLE grades (StudentID INT NOT NULL, ClassID INT, Grade TEXT);')
    cur.execute('CREATE TABLE classes (ClassID INT NOT NULL, Name TEXT);')
    # Adds info to tables
    with open('students.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO students VALUES (?, ?, ?, ?);', rows)
    with open('fields.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO majors VALUES (?, ?);', rows)
    with open('grades.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO grades VALUES (?, ?, ?);', rows)
    with open('classes.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO classes VALUES (?, ?);', rows)
    # Closes tables
    db.commit()

    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    query = "SELECT majors.name, COUNT(students.StudentID) FROM students LEFT OUTER JOIN majors ON students.majorcode=majors.ID GROUP BY majors.name"

    result = pd.read_sql_query(query, db)
    db.close()

    return result


def prob3():
    """
    Select students who received two or more non-Null grades in their classes.

    Return: A table of the students' names and the grades each received.
    """
    #Build your tables and/or query here
    db = sql.connect('sql')
    cur = db.cursor()
    # Creates tables
    cur.execute('DROP TABLE IF EXISTS students')
    cur.execute('DROP TABLE IF EXISTS majors')
    cur.execute('DROP TABLE IF EXISTS grades')
    cur.execute('DROP TABLE IF EXISTS classes')
    cur.execute('CREATE TABLE students (StudentID INT NOT NULL, Name TEXT, MajorCode INT, MinorCode INT);')
    cur.execute('CREATE TABLE majors (ID INT NOT NULL, Name TEXT);')
    cur.execute('CREATE TABLE grades (StudentID INT NOT NULL, ClassID INT, Grade TEXT);')
    cur.execute('CREATE TABLE classes (ClassID INT NOT NULL, Name TEXT);')
    # Adds info to tables
    with open('students.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO students VALUES (?, ?, ?, ?);', rows)
    with open('fields.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO majors VALUES (?, ?);', rows)
    with open('grades.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO grades VALUES (?, ?, ?);', rows)
    cur.execute("DELETE FROM grades WHERE (Grade=\'NULL\');")
    with open('classes.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO classes VALUES (?, ?);', rows)
    # Closes tables
    db.commit()
    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    query = "SELECT students.name, COUNT(grades.grade) FROM students LEFT OUTER JOIN grades ON students.StudentID=grades.StudentID GROUP BY students.studentID HAVING COUNT(*)>2;"

    result =  pd.read_sql_query(query, db)

    return result


def prob4():
    """
    Get the average GPA at the school using the given tables.

    Return: A float representing the average GPA, rounded to 2 decimal places.
    """
    db = sql.connect('sql')
    cur = db.cursor()
    # Creates tables
    cur.execute('DROP TABLE IF EXISTS students')
    cur.execute('DROP TABLE IF EXISTS majors')
    cur.execute('DROP TABLE IF EXISTS grades')
    cur.execute('DROP TABLE IF EXISTS classes')
    cur.execute('CREATE TABLE students (StudentID INT NOT NULL, Name TEXT, MajorCode INT, MinorCode INT);')
    cur.execute('CREATE TABLE majors (ID INT NOT NULL, Name TEXT);')
    cur.execute('CREATE TABLE grades (StudentID INT NOT NULL, ClassID INT, Grade TEXT);')
    cur.execute('CREATE TABLE classes (ClassID INT NOT NULL, Name TEXT);')
    # Adds info to tables
    with open('students.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO students VALUES (?, ?, ?, ?);', rows)
    with open('fields.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO majors VALUES (?, ?);', rows)
    with open('grades.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO grades VALUES (?, ?, ?);', rows)
    cur.execute("DELETE FROM grades WHERE (Grade=\'NULL\');")
    cur.execute("ALTER TABLE grades ADD gpa INTEGER;")
    with open('classes.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO classes VALUES (?, ?);', rows)
    # Closes tables
    db.commit()
    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    query = "INSERT INTO grades SELECT StudentID, ClassID, Grade, CASE Grade WHEN \'A+\' THEN 4.0 WHEN \'A-\' THEN 4.0 WHEN \'A\' THEN 4.0 "
    query += "WHEN \'B\' THEN 3.0 WHEN \'B-\' THEN 3.0 WHEN \'C\' THEN 2.0 WHEN \'C-\' THEN 2.0 WHEN \'C+\' THEN 2.0 "
    query += "WHEN \'D\' THEN 1.0 WHEN \'D-\' THEN 1.0 WHEN \'D+\' THEN 1.0"
    query += " END AS gpa FROM grades;"
    cur.execute(query)
    query = "SELECT AVG(gpa) FROM grades;"

    result =  pd.read_sql_query(query, db)

    return (int(result.values*100)/100.0)


def prob5():
    """
    Find all students whose last name begins with 'C' and their majors.

    Return: A table containing the names of the students and their majors.
    """
    #Build your tables and/or query here
    db = sql.connect('sql')
    cur = db.cursor()
    # Creates tables
    cur.execute('DROP TABLE IF EXISTS students')
    cur.execute('DROP TABLE IF EXISTS majors')
    cur.execute('DROP TABLE IF EXISTS grades')
    cur.execute('DROP TABLE IF EXISTS classes')
    cur.execute('CREATE TABLE students (StudentID INT NOT NULL, Name TEXT, MajorCode INT, MinorCode INT);')
    cur.execute('CREATE TABLE majors (ID INT NOT NULL, Name TEXT);')
    cur.execute('CREATE TABLE grades (StudentID INT NOT NULL, ClassID INT, Grade TEXT);')
    cur.execute('CREATE TABLE classes (ClassID INT NOT NULL, Name TEXT);')
    # Adds info to tables
    with open('students.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO students VALUES (?, ?, ?, ?);', rows)
    with open('fields.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO majors VALUES (?, ?);', rows)
    with open('grades.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO grades VALUES (?, ?, ?);', rows)
    with open('classes.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.executemany('INSERT INTO classes VALUES (?, ?);', rows)
    # Closes tables
    db.commit()
    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    query = "SELECT students.name, majors.name FROM students LEFT OUTER JOIN majors ON students.majorcode=majors.id WHERE students.name LIKE \'% C%\';"

    result =  pd.read_sql_query(query, db)

    return result
