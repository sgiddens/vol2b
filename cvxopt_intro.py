# cvxopt_intro.py
"""Volume 2B: CVXOPT
Spencer Giddens
Math 323
1/2/2016
"""

from cvxopt import matrix, solvers
import numpy as np
from scipy import linalg as la


def prob1():
    """Solve the following convex optimization problem:

    minimize        2x + y + 3z
    subject to      x + 2y          >= 3
                    2x + 10y + 3z   >= 10
                    x               >= 0
                    y               >= 0
                    z               >= 0

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (sol['primal objective'])
    """
    #raise NotImplementedError("Introductory problem not started.")
    c = matrix([2., 1., 3.])
    G = matrix([[-1., -2., -1., 0., 0.], [-2., -10., 0., -1., 0.], [0., -3., 0., 0., -1.]])
    h = matrix([-3., -10., 0., 0., 0.])
    sol = solvers.lp(c, G, h)
    return np.ravel(sol['x']), sol['primal objective']


# Problem 2
def l1Min(A, b):
    """Calculate the solution to the optimization problem

        minimize    ||x||_1
        subject to  Ax = b

    Parameters:
        A ((m,n) ndarray)
        b ((m, ) ndarray)

    Returns:
        The optimizer x (ndarray), without any slack variables u
        The optimal value (sol['primal objective'])
    """
    #raise NotImplementedError("L1 problem not started.")
    m, n = A.shape
    c = matrix(np.hstack((np.ones(n), np.zeros(n))))
    I = np.eye(n)
    G = np.hstack((-I, I))
    G = np.vstack((G, np.hstack((-I,-I))))
    G = matrix(G) # May need to expand G further to deal with equality (i. e. <= and >=)
    A = np.hstack((np.zeros((m,n)), A))
    A = matrix(A)
    h = matrix((np.zeros(2*n)))
    b = matrix(b)
    sol = solvers.lp(c, G, h, A, b) # May need to pass more constraints here to deal with equality
    return np.ravel(sol['x'])[n:], sol['primal objective']



def prob3():
    """Solve the transportation problem by converting the last equality constraint
    into inequality constraints.

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (sol['primal objective'])
    """
    #raise NotImplementedError("Transportation problem not started.")
    c = matrix([4., 7., 6., 8., 8., 9.])
    G = -1*np.eye(6)
    G = np.vstack((G, np.array([[1., 0., 1., 0., 1., 0.], [-1., 0., -1., 0., -1., 0.], [0., 1., 0., 1., 0., 1.], [0., -1., 0., -1., 0., -1.]])))
    G = matrix(G)
    h = np.zeros(6)
    h = np.hstack((h, 5., -5., 8., -8.))
    h = matrix(h)
    A = matrix(np.array([[1., 1., 0., 0., 0., 0.], [0., 0., 1., 1., 0., 0.], [0., 0., 0., 0., 1., 1.]]))
    b = matrix([7.,2.,4.])
    sol = solvers.lp(c, G, h, A, b)
    return np.ravel(sol['x']), sol['primal objective']

def prob4():
    """Find the minimizer and minimum of

    g(x,y,z) = (3/2)x^2 + 2xy + xz + 2y^2 + 2yz + (3/2)z^2 + 3x + z

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (sol['primal objective'])
    """
    #raise NotImplementedError("Quadratic minimization problem not started.")
    P = matrix(np.array([[3., 2., 1.],[2., 4., 2.],[1., 2., 3.]]))
    q = matrix([3.,0.,1.])
    sol = solvers.qp(P,q)
    return np.ravel(sol['x']), sol['primal objective']


# Problem 5
def l2Min(A, b):
    """Calculate the solution to the optimization problem

        minimize    ||x||_2
        subject to  Ax = b

    Parameters:
        A ((m,n) ndarray)
        b ((m, ) ndarray)

    Returns:
        The optimizer x (ndarray)
        The optimal value (sol['primal objective'])
    """
    #raise NotImplementedError("L2 problem not started.")
    m, n = A.shape
    P = matrix(2*np.eye(n))
    q = matrix(np.zeros(n))
    A = matrix(A)
    b = matrix(b)
    sol = solvers.qp(P,q,A=A,b=b)
    return np.ravel(sol['x']), sol['primal objective']


def prob6():
    """Solve the allocation model problem in 'ForestData.npy'.
    Note that the first three rows of the data correspond to the first
    analysis area, the second group of three rows correspond to the second
    analysis area, and so on.

    Returns (in order):
        The optimizer x (ndarray)
        The optimal value (sol['primal objective']*-1000)
    """
    #raise NotImplementedError("Forest problem not started.")
    A = np.load('ForestData.npy')
    c = matrix(-1*A[:,3])

    G = np.vstack((-1*A[:,4], np.vstack((-1*A[:,5], np.vstack((-1*A[:,6], -1*np.eye(21)))))))
    G = matrix(G)

    h = np.array([-40000., -5., -55160.])
    h = np.hstack((h, np.zeros(21)))
    h = matrix(h)

    a = np.array([[1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                  [0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                  [0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                  [0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                  [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.],
                  [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.],
                  [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.]])
    a = matrix(a)

    b = matrix(A[::3,1])

    sol = solvers.lp(c, G, h, a, b)
    return np.ravel(sol['x']), -1000*sol['primal objective']
