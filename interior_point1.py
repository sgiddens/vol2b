# Name this file 'solutions.py'.
"""Volume 2 Lab 19: Interior Point 1 (Linear Programs)
Spencer Giddens
Math 323
3/30/17
"""

import numpy as np
from scipy import linalg as la
from scipy.stats import linregress
from matplotlib import pyplot as plt


# Auxiliary Functions ---------------------------------------------------------
def startingPoint(A, b, c):
    """Calculate an initial guess to the solution of the linear program
    min c^T x, Ax = b, x>=0.
    Reference: Nocedal and Wright, p. 410.
    """
    # Calculate x, lam, mu of minimal norm satisfying both
    # the primal and dual constraints.
    B = la.inv(A.dot(A.T))
    x = A.T.dot(B.dot(b))
    lam = B.dot(A.dot(c))
    mu = c - A.T.dot(lam)

    # Perturb x and s so they are nonnegative.
    dx = max((-3./2)*x.min(), 0)
    dmu = max((-3./2)*mu.min(), 0)
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    # Perturb x and mu so they are not too small and not too dissimilar.
    dx = .5*(x*mu).sum()/mu.sum()
    dmu = .5*(x*mu).sum()/x.sum()
    x += dx*np.ones_like(x)
    mu += dmu*np.ones_like(mu)

    return x, lam, mu

# Use this linear program generator to test your interior point method.
def randomLP(m):
    """Generate a 'square' linear program min c^T x s.t. Ax = b, x>=0.
    First generate m feasible constraints, then add slack variables.
    Inputs:
        m -- positive integer: the number of desired constraints
             and the dimension of space in which to optimize.
    Outputs:
        A -- array of shape (m,n).
        b -- array of shape (m,).
        c -- array of shape (n,).
        x -- the solution to the LP.
    """
    n = m
    A = np.random.random((m,n))*20 - 10
    A[A[:,-1]<0] *= -1
    x = np.random.random(n)*10
    b = A.dot(x)
    c = A.sum(axis=0)/float(n)
    return A, b, -c, x

# This random linear program generator is more general than the first.
def randomLP2(m,n):
    """Generate a linear program min c^T x s.t. Ax = b, x>=0.
    First generate m feasible constraints, then add
    slack variables to convert it into the above form.
    Inputs:
        m -- positive integer >= n, number of desired constraints
        n -- dimension of space in which to optimize
    Outputs:
        A -- array of shape (m,n+m)
        b -- array of shape (m,)
        c -- array of shape (n+m,), with m trailing 0s
        v -- the solution to the LP
    """
    A = np.random.random((m,n))*20 - 10
    A[A[:,-1]<0] *= -1
    v = np.random.random(n)*10
    k = n
    b = np.zeros(m)
    b[:k] = A[:k,:].dot(v)
    b[k:] = A[k:,:].dot(v) + np.random.random(m-k)*10
    c = np.zeros(n+m)
    c[:n] = A[:k,:].sum(axis=0)/k
    A = np.hstack((A, np.eye(m)))
    return A, b, -c, v


# Problems --------------------------------------------------------------------
def interiorPoint(A, b, c, niter=20, tol=1e-16, verbose=False):
    """Solve the linear program min c^T x, Ax = b, x>=0
    using an Interior Point method.

    Parameters:
        A ((m,n) ndarray): Equality constraint matrix with full row rank.
        b ((m, ) ndarray): Equality constraint vector.
        c ((n, ) ndarray): Linear objective function coefficients.
        niter (int > 0): The maximum number of iterations to execute.
        tol (float > 0): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The optimal point.
        val (float): The minimum value of the objective function.
    """
    #raise NotImplementedError("Problems 1-4 Incomplete")
    # problem 1
    def F(x, l, mu):
        f1 = np.dot(A.T, l) + mu - c
        f2 = np.dot(A, x) - b
        f3 = np.dot(np.diag(mu), x)
        return np.hstack((f1, np.hstack((f2, f3))))

    # problem 2
    sigma = .1
    def DF(x, l, mu):
        M = np.diag(mu)
        X = np.diag(x)
        I = np.eye(len(mu))
        Znn = np.zeros_like(I)
        Zmn = np.zeros_like(A)
        Znm = np.zeros_like(A.T)
        Zmm = np.zeros_like(np.diag(l))
        row1 = np.hstack((Znn, np.hstack((A.T, I))))
        row2 = np.hstack((A, np.hstack((Zmm, Zmn))))
        row3 = np.hstack((M, np.hstack((Znm, X))))
        return np.vstack((row1, np.vstack((row2,row3))))

    def subroutine1(x, l, mu):
        Zn = np.zeros_like(mu)
        Zm = np.zeros_like(l)
        e = np.ones(len(mu))
        nu = np.dot(x, mu) / len(mu)
        vec = np.hstack((Zn,np.hstack((Zm, sigma*nu*e))))
        b = -1 * F(x,l,mu) + vec
        LU = la.lu_factor(DF(x,l,mu))
        return la.lu_solve(LU,b)

    # problem 3
    def subroutine2(x, l, mu):
        ret = subroutine1(x, l, mu)
        n = len(x)
        m = len(l)
        del_x = ret[:n]
        del_l = ret[n:n+m]
        del_mu = ret[n+m:]
        maskx = del_x < 0
        maskmu = del_mu < 0
        mu_vec = -mu / del_mu
        x_vec = -x / del_x
        alpha_max = min(1, np.min(mu_vec[maskmu]))
        delta_max = min(1, np.min(x_vec[maskx]))
        alpha = min(1, .95*alpha_max)
        delta = min(1, .95*delta_max)
        return alpha, delta

    # problem 4
    x, l, mu = startingPoint(A,b,c)
    it = 0
    flag = False
    while np.dot(x, mu) / len(mu) >= tol:
        it += 1
        if it > niter:
            flag = True
            break
        alpha, delta = subroutine2(x, l, mu)
        ret = subroutine1(x, l, mu)
        n = len(x)
        m = len(l)
        del_x = ret[:n]
        del_l = ret[n:n+m]
        del_mu = ret[n+m:]
        x = x + delta*del_x
        l = l + alpha*del_l
        mu = mu + alpha*del_mu
    return x, np.dot(c,x)


def leastAbsoluteDeviations(filename='simdata.txt'):
    """Generate and show the plot requested in the lab."""
    #raise NotImplementedError("Problem 5 Incomplete")
    data = np.loadtxt(filename)
    m = data.shape[0]
    n = data.shape[1] - 1
    c = np.zeros(3*m + 2*(n+1))
    c[:m] = 1
    y = np.empty(2*m)
    y[::2] = -data[:,0]
    y[1::2] = data[:,0]
    x = data[:,1:]


    A = np.ones((2*m,3*m + 2*(n+1)))
    A[::2,:m] = np.eye(m)
    A[1::2,:m] = np.eye(m)
    A[::2,m:m+n] = -x
    A[1::2,m:m+n] = x
    A[::2, m+n:m+2*n] = x
    A[1::2, m+n:m+2*n] = -x
    A[::2, m+2*n] = -1
    A[1::2, m+2*n+1] = -1
    A[:, m+2*n+2:] = -np.eye(2*m, 2*m)

    sol = interiorPoint(A,y,c,niter=10)[0]

    beta = sol[m:m+n] - sol[m+n:m+2*n]
    b = sol[m+2*n] - sol[m+2*n+1]

    slope, intercept = linregress(data[:,1], data[:,0])[:2]
    domain = np.linspace(0,10,200)
    plt.plot(domain,domain*slope + intercept, label='Least Squares')
    plt.plot(domain,(domain*beta) + b, label='Least Absolute Deviation')
    plt.plot(x, y[1::2], 'k*')
    plt.legend()
    plt.show()
