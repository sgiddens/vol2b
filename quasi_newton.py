# quasi_newton.py
"""Volume 2: Quasi-Newton Methods
Spencer Giddens
Math 323
2/22/17
"""

import numpy as np
from scipy import linalg as la
import time
from scipy.optimize import leastsq
from matplotlib import pyplot as plt

def newton_ND(J, H, x0, niter=10, tol=1e-5):
    """
    Perform Newton's method in N dimensions.

    Inputs:
        J (function): Jacobian of the function f for which we are finding roots.
        H (function): Hessian of f.
        x0 (float): The initial guess.
        niter (int): Number of iterations to compute.
        tol (float): Stopping criteria for iterations.

    Returns:
        The approximated root and the number of iterations it took.
    """
    #return NotImplementedError("Problem 1 Incomplete")
    xold = x0
    i = 0
    flag = False
    while i < niter:
        i += 1
        xnew = xold - la.solve(H(xold),J(xold).T)
        if la.norm(xnew - xold) < tol:
            flag = True
            break
        else:
            xold = xnew
    return xnew, i


def broyden_ND(J, H, x0, niter=20, tol=1e-5):
    """
    Perform Broyden's method in N dimensions.

    Inputs:
        J (function): Jacobian of the function f for which we are finding roots.
        H (function): Hessian of f.
        x0 (float): The initial guess.
        niter (int): Number of iterations to compute.
        tol (float): Stopping criteria for iterations.

    Returns:
        The approximated root and the number of iterations it took.
    """
    #return NotImplementedError("Problem 2 Incomplete")
    xold = x0
    i = 0
    A = la.inv(H(x0))
    flag = False
    while i < niter:
        i += 1
        xnew = xold - np.dot(A, J(xold).T)
        y = J(xnew).T - J(xold).T
        s = xnew - xold
        A = A + np.outer(s - np.dot(A,y), np.dot(s.T,A)) / np.dot(np.dot(s.T,A),y)
        if la.norm(xnew - xold) < tol:
            flag = True
            break
        else:
            xold = xnew
    return xnew, i


def BFGS(J, H, x0, niter=10, tol=1e-6):
    """
    Perform BFGS in N dimensions.

    Inputs:
        J (function): Jacobian of objective function.
        H (function): Hessian of objective function.
        x0 (float): The initial guess.
        niter (int): Number of iterations to compute.
        tol (float): Stopping criteria for iterations.

    Returns:
        The approximated root and the number of iterations it took.
    """
    #return NotImplementedError("Problem 3 Incomplete")
    xold = x0
    i = 0
    A = la.inv(H(x0))
    flag = False
    while i < niter:
        i += 1
        xnew = xold - np.dot(A, J(xold).T)
        y = J(xnew).T - J(xold).T
        s = xnew - xold
        sTy = np.dot(s.T,y)
        X = (sTy + np.dot(y.T,np.dot(A,y))) * np.outer(s,s.T) / sTy**2
        Y = (np.dot(A,np.outer(y,s.T)) + np.dot(np.outer(s,y.T), A)) /sTy
        A = A + X - Y
        if la.norm(xnew - xold) < tol:
            flag = True
            break
        else:
            xold = xnew
    return xnew, i

def prob4():
    """
    Compare the performance of Newton's, Broyden's, and modified Broyden's
    methods on the following functions:
        f(x,y) = 0.26(x^2 + y^2) - 0.48xy
        f(x,y) = sin(x + y) + (x - y)^2 - 1.5x + 2.5y + 1
    """
    #return NotImplementedError("Problem 4 Incomplete")
    df = lambda x: np.array([.52*x[0] - .48*x[1], .52*x[1] - .48*x[0]])
    hf = lambda x: np.array([[.52, -.48],[-.48,.52]])
    dg = lambda x: np.array([np.cos(x[0] + x[1]) +2*(x[0] - x[1]) - 1.5, np.cos(x[0] + x[1]) - 2*(x[0] - x[1]) + 2.5])
    hg = lambda x: np.array([[-np.sin(x[0] + x[1]) + 2, -np.sin(x[0] + x[1]) - 2],
                             [-np.sin(x[0] + x[1]) - 2, -np.sin(x[0] + x[1]) + 2]])
    x0 = np.array([2,2])
    x1 = np.array([3,0])
    L = [(df, hf, x0), (dg, hg, x1)]



    # Newton
    for i, tup in enumerate(L):
        start = time.time()
        it = newton_ND(tup[0], tup[1], tup[2])[1]
        t = time.time() - start
        print "Newton's method:"
        print "Function:", i+1
        print "Iterations:", it
        print "Time:", t
        print "Time per iteration:", t / it
        print '\n'

    for i,tup in enumerate(L):
        start = time.time()
        it = broyden_ND(tup[0], tup[1], tup[2])[1]
        t = time.time() - start
        print "Broyden's method:"
        print "Function", i+1
        print "Iterations:", it
        print "Time:", t
        print "Time per iteration:", t / it
        print '\n'

    for i, tup in enumerate(L):
        start = time.time()
        it = BFGS(tup[0], tup[1], tup[2])[1]
        t = time.time() - start
        print "BFGS method:"
        print "Function", i+1
        print "Iterations:", it
        print "Time:", t
        print "Time per iteration:", t / it
        print '\n'


def gauss_newton(J, r, x0, niter=10):
    """
    Solve a nonlinear least squares problem with Gauss-Newton method.

    Inputs:
        J (function): Jacobian of the objective function.
        r (function): Residual vector.
        x0 (float): The initial guess.
        niter (int): Number of iterations to compute.

    Returns:
        The approximated root.
    """
    #return NotImplementedError("Problem 5 Incomplete")
    xold = x0
    i = 0
    while i < niter:
        i += 1
        xnew = xold - la.solve(np.dot(J(xold).T, J(xold)),np.dot(J(xold).T, r(xold)))
        xold = xnew
    return xnew


def prob6():
    """
    Compare the least squares regression with 8 years of population data and 16
    years of population data.
    """
    #return NotImplementedError("Problem 6 Incomplete")
    #Start with the first 8 decades of data
    years1 = np.arange(8)
    pop1 = np.array([3.929, 5.308, 7.240, 9.638, 12.866,
    17.069, 23.192, 31.443])
    # Now consider the first 16 decades
    years2 = np.arange(16)
    pop2 = np.array([3.929, 5.308, 7.240, 9.638, 12.866,
    17.069, 23.192, 31.443, 38.558, 50.156,
    62.948, 75.996, 91.972, 105.711, 122.775,
    131.669])

    x0 = np.array([1.5,.5,2.5])

    def model1(x, t):
        return x[0]*np.exp(x[1]*(t+x[2]))
    def residual1(x):
        return model1(x, years1) - pop1
    x2 = leastsq(residual1, x0)[0]
    dom= np.linspace(0,10,500)
    plt.subplot(121)
    plt.plot(years1, pop1, '*')
    plt.plot(dom, model1(x2, dom))

    x0 = np.array([1.5,.5,-20.])
    def model2(x, t):
        return x[0]/(1+np.exp(-x[1]*(t+x[2])))
    def residual2(x):
        return model2(x,years2) - pop2
    x2 = leastsq(residual2, x0)[0]

    dom = np.linspace(0,20,500)

    plt.subplot(122)
    plt.plot(years2, pop2, '*')
    plt.plot(dom,model2(x2,dom))
    plt.show()
