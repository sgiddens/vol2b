# one_dimensional_optimization.py
"""Volume 2B: 1-D Optimization.
Spencer Giddens
Math 323
1/9/17
"""
import numpy as np
import math


# Problem 1
def golden_section(f, a, b, niter=10):
    """Find the minimizer of the unimodal function f on the interval [a,b]
    using the golden section search method.

    Inputs:
        f (function): unimodal scalar-valued function on R.
        a (float): left bound of the interval of interest.
        b (float): right bound of the interval of interest.
        niter (int): number of iterations to compute.

    Returns:
        the approximated minimizer (the midpoint of the final interval).
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    if niter==0:
        print "It takes 13 steps to get within .001 of the true minimizer."
        return (a+b)/2
    else:
        row =(3-math.sqrt(5))/2
        aprime, bprime = a+row*(b-a), a+(1-row)*(b-a)
        if f(aprime) >= f(bprime):
            return golden_section(f, aprime, b, niter-1)
        else:
            return golden_section(f, a, bprime, niter-1)




# Problem 2
def bisection(df, a, b, niter=10):
    """Find the minimizer of the unimodal function with derivative df on the
    interval [a,b] using the bisection algorithm.

    Inputs:
        df (function): derivative of a unimodal scalar-valued function on R.
        a (float): left bound of the interval of interest.
        b (float): right bound of the interval of interest.
        niter (int): number of iterations to compute.
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    x = (a+b)/2.
    if niter==0:
        print "It takes only 9 iterations to get to within .001 of the minimizer. The bisection algorithm is faster to converge."
        return x
    else:
        if df(x) > 0:
            return bisection(df, a, x, niter-1)
        else:
            return bisection(df, x, b, niter-1)


# Problem 3
def newton1d(f, df, ddf, x, niter=10):
    """Minimize the scalar function f with derivative df and second derivative
    df using Newton's method.

    Parameters
        f (function): A twice-differentiable scalar-valued function on R.
        df (function): The first derivative of f.
        ddf (function): The second derivative of f.
        x (float): The initial guess.
        niter (int): number of iterations to compute.

    Returns:
        The approximated minimizer.
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    if niter==0:
        print "For me, he function failed when the initial guess was zero, but was successful when the initial guess was -np.pi/6."
        return x
    else:
        xnew = x - (df(x)/ddf(x))
        return newton1d(f,df,ddf,xnew, niter-1)


# Problem 4
def secant1d(f, df, x0, x1, niter=10):
    """Minimize the scalar function f using the secant method.

    Inputs:
        f (function): A differentiable scalar-valued function on R.
        df (function): The first derivative of f.
        x0 (float): A first initial guess.
        x1 (float): A second initial guess.
        niter (int): number of iterations to compute.

    Returns:
        The approximated minimizer.
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    if niter==0:
        print "It fails when x0 = 2 and x1 = 1. The function is very volatile so a small change in initial guesses results in a large change in output."
        return x1
    else:
        xnew = x1 - ((x1 - x0)/(df(x1) - df(x0)))*df(x1)
        return secant1d(f,df,x1, xnew,niter-1)


# Problem 5
def backtracking(f, slope, x, p, a=1, rho=.9, c=10e-4):
    """Do a backtracking line search to satisfy the Wolfe Conditions.
    Return the step length.

    Inputs:
        f (function): A scalar-valued function on R.
        slope (float): The derivative of f at x.
        x (float): The current approximation to the minimizer.
        p (float): The current search direction.
        a (float): Initial step length (set to 1 in Newton and quasi-Newton
            methods).
        rho (float): Parameter in (0,1).
        c (float): Parameter in (0,1).

    Returns:
        The computed step size.
    """
    #raise NotImplementedError("Problem 5 Incomplete")
    while f(x + a*p) > f(x) + c*a*slope*p:
        a = rho*a
    return a
