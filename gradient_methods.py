# gradient_methods.py
"""Volume 2: N-D Optimization 2 (Gradient Descent Methods).
Spencer Giddens
Math 323
3/6/17
"""

import numpy as np
from scipy import linalg as la
import scipy.optimize as opt

# Problem 1
def steepestDescent(f, Df, x0, step=1, tol=.0001, maxiters=50):
    """Use the Method of Steepest Descent to find the minimizer x of the convex
    function f:Rn -> R.

    Parameters:
        f (function Rn -> R): The objective function.
        Df (function Rn -> Rn): The gradient of the objective function f.
        x0 ((n,) ndarray): An initial guess for x.
        step (float): The initial step size.
        tol (float): The convergence tolerance.

    Returns:
        x ((n,) ndarray): The minimizer of f.
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    i = 0
    x = x0
    flag = False
    while i < maxiters:
        i += 1
        step = 1.
        while f(x - step*Df(x)) >= f(x):
            step = step / 2.
            if step < tol:
                flag = True
                break
        if flag:
            break
        else:
            x = x - step*Df(x)
    return x


# Problem 2
def conjugateGradient(b, x0, Q, tol=.0001):
    """Use the Conjugate Gradient Method to find the solution to the linear
    system Qx = b.

    Parameters:
        b  ((n, ) ndarray)
        x0 ((n, ) ndarray): An initial guess for x.
        Q  ((n,n) ndarray): A positive-definite square matrix.
        tol (float): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The solution to the linear systm Qx = b, according
            to the Conjugate Gradient Method.
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    x = x0
    rold = np.dot(Q,x) - b
    d = -rold
    k = 0
    while la.norm(rold) >= tol:
        step = np.dot(rold,rold) / np.dot(d, np.dot(Q,d))
        x = x + step*d
        rnew = rold + step*np.dot(Q,d)
        beta = np.dot(rnew, rnew) / np.dot(rold,rold)
        d = -rnew + beta*d
        rold = rnew
    return x


# Problem 3
def prob3(filename="linregression.txt"):
    """Use conjugateGradient() to solve the linear regression problem with
    the data from linregression.txt.
    Return the solution x*.
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    stuff = np.loadtxt(filename)
    b = stuff[:,0]
    A = np.hstack((np.ones((len(stuff), 1)), stuff[:, 1:]))
    Q = np.dot(A.T,A)
    x0 = np.random.random(len(Q))
    b0 = np.dot(A.T, b)
    x = conjugateGradient(b0,x0,Q)
    return x

# Problem 4
def prob4(filename="logregression.txt"):
    """Use scipy.optimize.fmin_cg() to find the maximum likelihood estimate
    for the data in logregression.txt.
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    stuff = np.loadtxt(filename)
    y = stuff[:,0]
    x = np.hstack((np.ones((len(stuff),1)),stuff[:,1:]))
    def objective(b):
        return (np.log(1+np.exp(x.dot(b))) - y*(x.dot(b))).sum()
    guess = np.ones(4)
    b0 = opt.fmin_cg(objective,guess)
    return b0
