# simplex.py
"""Volume 2B: Simplex.
Spencer Giddens
Math 323
1/16/2017

Problems 1-6 give instructions on how to build the SimplexSolver class.
The grader will test your class by solving various linear optimization
problems and will only call the constructor and the solve() methods directly.
Write good docstrings for each of your class methods and comment your code.

prob7() will also be tested directly.
"""

import numpy as np
import sys

# Problems 1-6
class SimplexSolver(object):
    """Class for solving the standard linear optimization problem

                        maximize        c^Tx
                        subject to      Ax <= b
                                         x >= 0
    via the Simplex algorithm.
    """

    def __init__(self, c, A, b):
        """

        Parameters:
            c (1xn ndarray): The coefficients of the linear objective function.
            A (mxn ndarray): The constraint coefficients matrix.
            b (1xm ndarray): The constraint vector.

        Raises:
            ValueError: if the given system is infeasible at the origin.
        """
        #return NotImplementedError("SimplexSolver Not Started")
        Ax = np.zeros_like(b)
        if not all(Ax<=b):
            raise ValueError("The system is infeasible at the origin")
        else:
            self.c = c
            self.A = A
            self.b = b
            m, n = A.shape
            self.m = m
            self.n = n
            self.initSlackVariables()
            self.initTableau()

    # Problem 2
    def initSlackVariables(self):
        """
        Initializes the slack variables and stores them and the original
        variables as a numpy array, var.
        Initializes L as the index list. The first m entries are the basic
        variables, and the last n entries are the nonbasic variables.
        """
        self.var = np.array(list(self.b) + list(np.zeros(self.n)))
        L = range(self.m+self.n) # lists 0-(m+n)
        self.L = L[self.n:] + L[:self.n] # rearranges list

    # Problem 3
    def initTableau(self):
        """
        Initializes the tableau, T, as a numpy array.
        """
        Abar = np.hstack((self.A, np.eye(self.m)))
        cbar = np.hstack((self.c, np.zeros(self.m)))
        b = self.b.reshape((self.m, 1))
        zero_vector = np.zeros_like(b)
        Ttop = np.hstack((np.array([0]), np.hstack((-1*cbar, np.array([1])))))
        Tbottom = np.hstack((b, np.hstack((Abar, zero_vector))))
        self.T = np.vstack((Ttop, Tbottom))

    # Problem 4
    def BlandsRule(self):
        """
        Uses Bland's Rule to determine the pivot row, i, and the pivot
        column, j. Returns them if found
        If all entries in the pivot column are non-positive,
        it raises a ValueError.
        If all entries on the first row (except first column) are
        non-negative, returns -1.
        """
        if all(self.T[0,1:] >= 0):
            return -1
        j = 1
        while self.T[0,1:][j-1] >= 0:
            j += 1
        entering_index = j - 1 # This is the value in L
        pivot_column = j
        entering_index = self.L.index(entering_index) # This is the index of the value
        # Raises -1 if pivot column all non-positive
        if all(self.T[1:,j] <= 0):
            raise ValueError("The problem is unbounded")
        # At least one of the entries is positive
        if not any(self.T[1:,j] == 0):
            ratios = self.T[1:,0] / self.T[1:,j] # If there won't be division by 0
        else:
            ratios = []
            for i, zero in enumerate(self.T[1:,j] == 0):
                if zero:
                    ratios.append(sys.maxsize)
                else:
                    ratios.append(self.T[1:,0][i] / self.T[1:,j][i])
            ratios = np.array(ratios)
        # Removes negative entries
        for i in xrange(len(ratios)):
            if ratios[i] < 0:
                ratios[i] = sys.maxsize
        # Finds index of minimum ratio. If there are 2, returns the first,
        # as per Bland's Rule. Adds 1 to account for top row of T, but
        # subtracts one to account for row i corresponding to L_i-1.
        leaving_index = np.argmin(ratios)

        # Swap entries in L
        L = self.L
        L[entering_index], L[leaving_index] = L[leaving_index], L[entering_index]
        self.L = L

        pivot_row = leaving_index + 1
        return pivot_row, pivot_column

    # Problem 5
    def pivot(self):
        """
        Runs BlandsRule to find appropriate indices. If algorithm should
        terminate, returns False. If algorithm should continue, adjusts T
        appropriately, then returns True.
        """
        if self.BlandsRule() == -1:
            return False
        else:
            i, j = self.BlandsRule()
            T = self.T
            T[i] = T[i] / T[i][j]
            for row in xrange(len(T[:i])):
                T[row] = T[row] + (T[i] * -T[row][j])
            for row in xrange(len(T[i+1:])):
                T[row+i+1] = T[row+i+1] + (T[i] * -T[row+i+1][j])
            self.T = T
            return True

    # Problem 6
    def solve(self):
        """Solve the linear optimization problem.

        Returns:
            (float) The maximum value of the objective function.
            (dict): The basic variables and their values.
            (dict): The nonbasic variables and their values.
        """
        #raise NotImplementedError("SimplexSolver Incomplete")
        while self.pivot():
            pass
        d1 = dict()
        d2 = dict()
        for i in xrange(self.m):
            d1[self.L[i]] = self.T[i+1][0]
        for i in xrange(self.n):
            d2[self.L[i+self.m]] = 0
        return self.T[0][0], d1, d2


# Problem 7
def prob7(filename='productMix.npz'):
    """Solve the product mix problem for the data in 'productMix.npz'.

    Parameters:
        filename (str): the path to the data file.

    Returns:
        The minimizer of the problem (as an array).
    """
    #raise NotImplementedError("Problem 7 Incomplete")
    thing = np.load(filename)
    A = thing['A']
    p = thing['p']
    m = thing['m']
    d = thing['d']
    a = np.vstack((A, np.eye(len(d))))
    b = np.hstack((m, d))
    solver = SimplexSolver(p, a, b)
    sol = solver.solve()
    d1 = sol[1]
    d2 = sol[2]
    for item in d2.iterkeys():
        d1[item] = d2[item]
    ret_list = []
    for i in xrange(len(d)):
        ret_list.append(d1[i])
    return np.array(ret_list)
