# scipy_optimize_intro.py
"""Volume 2B: Optimization with Scipy
Spencer Giddens
Math 323
1/18/2017
"""
import scipy.optimize as opt
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from blackbox_function import blackbox



# Problem 1
def prob1():
    """Use the minimize() function in the scipy.optimize package to find the
    minimum of the Rosenbrock function (scipy.optimize.rosen) using the
    following methods:
        Nelder-Mead
        CG
        BFGS
    Use x0 = np.array([4., -2.5]) for the initial guess for each test.

    For each method, print whether it converged, and if so, print how many
        iterations it took.
    """
    #return NotImplementedError("Problem 1 not implemented")
    x0 = np.array([4.,-2.5])
    for method in ['Nelder-Mead', 'CG', 'BFGS']:
        res = opt.minimize(opt.rosen, x0, method=method, jac=opt.rosen_der)
        print method
        print "Converged?", res['success']
        if res['success']:
            print "Number of iterations:", res['nit']


# Problem 2
def prob2():
    """Minimize the function blackbox() in the blackbox_function module,
    selecting the appropriate method of scipy.optimize.minimize() for this
    problem.  Do not pass your method a derivative. You may need to test
    several methods and determine which is most appropriate.

    The blackbox() function returns the length of a piecewise-linear curve
    between two fixed points: the origin, and the point (40,30).
    It accepts a one-dimensional ndarray} of length m of y-values, where m
    is the number of points of the piecewise curve excluding endpoints.
    These points are spaced evenly along the x-axis, so only the y-values
    of each point are passed into blackbox().

    Once you have selected a method, select an initial point with the
    provided code.

    Plot your initial curve and minimizing curve together on the same
    plot, including endpoints. Note that this will require padding your
    array of internal y-values with the y-values of the endpoints, so
    that you plot a total of 20 points for each curve.
    """
    #return NotImplementedError("Problem 2 not implemented")
    x0 = 30*np.random.random_sample(18)
    y_values = np.zeros(20)
    for i in xrange(len(x0)):
        y_values[i+1] = x0[i]
    y_values[19] = 30.
    for method in ['BFGS']:
        res = opt.minimize(blackbox, x0, method=method)
        print method
        print "Converged?", res['success']
        if res['success']:
            print "Number of iterations:", res['nit']
        y_min = res['x']
    y_min1 = np.zeros(20)
    for i in xrange(len(y_min)):
        y_min1[i+1] = y_min[i]
    y_min1[19] = 30.
    plt.plot(np.linspace(0,40,20), y_min1)
    plt.plot(np.linspace(0,40,20), y_values)
    plt.show()

# Problem 3
def prob3():
    """Explore the documentation on the function scipy.optimize.basinhopping()
    online or via IPython. Use it to find the global minimum of the multmin()
    function given in the lab, with initial point x0 = np.array([-2, -2]) and
    the Nelder-Mead algorithm. Try it first with stepsize=0.5, then with
    stepsize=0.2.

    Plot the multimin function and minima found using the code provided.
    Print statements answering the following questions:
        Which algorithms fail to find the global minimum?
        Why do these algorithms fail?

    Finally, return the global minimum.
    """
    #return NotImplementedError("Problem 3 not implemented")
    def multimin(x):
        r = np.sqrt((x[0]+1)**2 + x[1]**2)
        return r**2 * (1+np.sin(4*r)**2)
    x0 = np.array([-2,-2])
    for s in [0.5, 0.2]:
        res = opt.basinhopping(multimin, x0, stepsize=s, minimizer_kwargs={'method':'nelder-mead'})
        x = res['x']
        if s == 0.5:
            xreal = x
        print str(s) + ':',x

    #Plot
    xdomain = np.linspace(-3.5,1.5, 70)
    ydomain =  np.linspace(-2.5,2.5,60)
    X,Y = np.meshgrid(xdomain, ydomain)
    Z = multimin((X,Y))
    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection = '3d')
    ax1.plot_wireframe(X,Y,Z, linewidth=.5, color='c')
    # Plot values
    ax1.scatter(x0[0], x0[1], multimin(x0))
    ax1.scatter(x[0], x[1], multimin(x))
    ax1.scatter(xreal[0], xreal[1], multimin(xreal))

    plt.show()

    print "The algorithm fails with stepsize 0.2 because the stepsize is too small and it doesn't escape the valley the initial point is in."
    return multimin(xreal)




# Problem 4
def prob4():
    """Find the roots of the function
               [       -x + y + z     ]
    f(x,y,z) = [  1 + x^3 - y^2 + z^3 ]
               [ -2 - x^2 + y^2 + z^2 ]

    Returns the values of x,y,z as an array.
    """
    #return NotImplementedError("Problem 4 not implemented")
    def func(x):
        return np.array([-x[0] + x[1] + x[2], 1 + x[0]**3 - x[1]**2 + x[2]**3, -2 - x[0]**2 + x[1]**2 + x[2]**2])
    def jac(x):
        return np.array([[-1, 1, 1],
                         [3*x[0]**2, -2*x[1], 3*x[2]**2],
                         [-2*x[0], 2*x[1], 2*x[2]]])
    sol = opt.root(func, [0,0,0], jac=jac, method='hybr')
    return np.array(sol.x)


# Problem 5
def prob5():
    """Use the scipy.optimize.curve_fit() function to fit a curve to
    the data found in `convection.npy`. The first column of this file is R,
    the Rayleigh number, and the second column is Nu, the Nusselt number.

    The fitting parameters should be c and beta, as given in the convection
    equations.

    Plot the data from `convection.npy` and the curve generated by curve_fit.
    Return the values c and beta as an array.
    """
    #return NotImplementedError("Problem 5 not implemented")
    def convection(R,c,beta):
        return c*R**beta

    A = np.load('convection.npy')
    x, y = A[:,0], A[:,1]
    popt, pcov = opt.curve_fit(convection, x[0:], y[0:])

    c = popt[0]
    beta = popt[1]
    plt.loglog(x,y, 'k.')
    x_values = np.linspace(2e3, 2e8,1e3)
    plt.loglog(x_values, convection(x_values, popt[0], popt[1]), 'b-')

    plt.show()
    return popt
